package cat.inspedralbes.povfra;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
//import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class PovFraUF1A1EncryptData {
	
	public static final byte[] IV_PARAM_16 = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F}; // IV definition. AES: 16 bytes (one block)
	public static final byte[] IV_PARAM_8  = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07}; // IV definition. DES: 8 bytes 
	
	public static void main( String[] args) throws IOException {

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		
		//CONTINGUT FITXER PROCESSAR
		String contingutFitxerProcessar = "Hola nois!";
		
		//PADDING
		String tipusPadding = "PKCS5Padding";
		
		//ALGORISME DE XIFRAT
		String tipusAlgorisme = "AES";
//		String tipusAlgorisme = "DES";
		
		//LONGITUD DE LA CLAU SECRETA
		int longitudClau = 128;
//		int longitudClau = 192;
//		int longitudClau = 256;
		
		//ALGORISME DE HASH
		String algorismeHash = "MD5";
//		String algorismeHash = "SHA-1";
//		String algorismeHash = "SHA-256";
		
		//TIPUS DE XIFRAT PER BLOCS
		String tipusXifrat = "ECB";
//		String tipusXifrat = "CBC";
		
		//FITXER A PROCESSAR
		File fitxerProcessar = new File("fitxerProcessar.txt"); 
		
		if (fitxerProcessar.createNewFile()) {
			System.out.println("S'ha creat el fitxer " + fitxerProcessar.getName());	
		}
		PovFraUF1A1File.writeFile(contingutFitxerProcessar.getBytes(), "fitxerProcessar.txt");
		System.out.println("S'ha escrit al fitxer " + fitxerProcessar.getName() + " el text " + "'" + contingutFitxerProcessar + "'");	
		
		File fitxerProcessarCry = null;
		
		//PARAULA O TEXT PER GENERAR LA CLAU SECRETA
		SecretKey clau = null;
		if( tipusXifrat.equals("ECB") ) {
			clau = passwordKeyGeneration("atenea", longitudClau, algorismeHash, tipusAlgorisme);			
		}else if( tipusXifrat.equals("CBC") ) {
			clau = keygenKeyGeneration(longitudClau, tipusAlgorisme);			
		}
		
		//Comprovar si existeix el fitxer i, si existeix, llegir i 
		//encriptar el contingut copiant-lo en un fitxer encriptat.
		if( fitxerProcessar.exists() ) {			
	        try {
	        	
	        	byte[] contingutFitxer = PovFraUF1A1File.readFile(fitxerProcessar);
	        	fitxerProcessarCry = new File("fitxerProcessar.cry");
				
				byte[] encriptat = transformData(clau, contingutFitxer, Cipher.ENCRYPT_MODE, tipusAlgorisme, tipusXifrat, tipusPadding);
				PovFraUF1A1File.writeFile(encriptat, "fitxerProcessar.cry");
	        	
				PovFraUF1A1File.wipe(fitxerProcessar);
				if (fitxerProcessar.delete()) {
					System.out.println("S'ha eliminat el fitxer " + fitxerProcessar.getName() + "\n");	
				}
				
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
		}	
		
		System.out.println("Vols continuar? Persiona \"enter\" continuar...");
		scanner.nextLine();
		
		//Comprovar si existeix el fitxer encriptat i, si existeix, llegir i 
		//desencriptar el contingut copiant-lo en un fitxer normal.
		if( fitxerProcessarCry.exists()) {			
	        try {
	        	
	        	byte[] contingutFitxer = PovFraUF1A1File.readFile(fitxerProcessarCry);
	        	fitxerProcessar = new File("fitxerProcessar.txt");
				
				byte[] desencriptar = transformData(clau, contingutFitxer, Cipher.DECRYPT_MODE, tipusAlgorisme, tipusXifrat, tipusPadding);
				PovFraUF1A1File.writeFile(desencriptar, "fitxerProcessar.txt");
				
				PovFraUF1A1File.wipe(fitxerProcessarCry);
				if (fitxerProcessarCry.delete()) {
					System.out.println("S'ha eliminat el fitxer " + fitxerProcessarCry.getName());	
				}
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
		}
		
	}
	
	public static byte[] transformData(SecretKey sKey, byte[] data, int mode, String tipusAlgorisme, String tipusXifrat, String tipusPadding) {
		 byte[] transformData = null;
		 
		 try {
			 
	 		 IvParameterSpec iv = new IvParameterSpec(IV_PARAM_16);
			 if( tipusAlgorisme.equals("DES") ) {
		 		 iv = new IvParameterSpec(IV_PARAM_8); 
			 }

			 Cipher cipher =Cipher.getInstance(tipusAlgorisme+"/"+tipusXifrat+"/"+tipusPadding);	
			 
			 if( tipusXifrat.equals("ECB") ) {
				cipher.init(mode, sKey);				 
			 }else if(tipusXifrat.equals("CBC")) {
				cipher.init(mode, sKey, iv);
			 }

			 transformData = cipher.doFinal(data);
		 }
		 catch (Exception ex) {
			 System.err.println("Error xifrant les dades: " + ex);
		 }
		 
		 return transformData;
	}
	
	public static SecretKey passwordKeyGeneration(String text, int keySize, String algorismeHash, String tipusAlgorisme) {
		
		SecretKey sKey = null;
		
		if ((keySize == 64)||(keySize == 128)||(keySize == 192)||(keySize == 256)){
			try {
				byte[] data = text.getBytes("UTF-8");
				MessageDigest md = MessageDigest.getInstance(algorismeHash);
				byte[] hash = md.digest(data);
				byte[] key = Arrays.copyOf(hash, keySize/8);
				sKey = new SecretKeySpec(key, tipusAlgorisme); 
			}
			catch (Exception ex) {
				System.err.println("Error generant la clau:" + ex); 
			}
		}
		return sKey;
	}
	
	public static SecretKey keygenKeyGeneration(int keySize, String tipusAlgorisme) {
		
		SecretKey sKey = null;
		
		if ((keySize == 64)||(keySize == 128)||(keySize == 192)||(keySize == 256)) {
			try {
				KeyGenerator kgen = KeyGenerator.getInstance(tipusAlgorisme);
				kgen.init(keySize);
				sKey = kgen.generateKey();
			}
			catch (NoSuchAlgorithmException ex){
				System.err.println("Generador no disponible.");
			}
		}
		
		return sKey;
	}
}
