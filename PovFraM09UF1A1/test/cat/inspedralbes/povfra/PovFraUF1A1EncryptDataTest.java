package cat.inspedralbes.povfra;

import static org.junit.jupiter.api.Assertions.*;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class PovFraUF1A1EncryptDataTest {

	@ParameterizedTest
	@CsvSource({
		"PKCS5Padding,DES,64,MD5,ECB",
		"PKCS5Padding,DES,64,SHA-1,ECB",
		"PKCS5Padding,DES,64,SHA-256,ECB",
		"PKCS5Padding,AES,128,MD5,ECB", 
		"PKCS5Padding,AES,128,SHA-1,ECB",
		"PKCS5Padding,AES,128,SHA-256,ECB",
		"PKCS5Padding,AES,192,SHA-256,ECB",
		"PKCS5Padding,AES,256,SHA-256,ECB",
		
		"PKCS5Padding,DES,64,MD5,CBC",
		"PKCS5Padding,DES,64,SHA-1,CBC",
		"PKCS5Padding,DES,64,SHA-256,CBC",
		"PKCS5Padding,AES,128,MD5,CBC", 
		"PKCS5Padding,AES,128,SHA-1,CBC",
		"PKCS5Padding,AES,128,SHA-256,CBC",
		"PKCS5Padding,AES,192,SHA-256,CBC",
		"PKCS5Padding,AES,256,SHA-256,CBC"
		})
	
	void test(String tipusPadding, String tipusAlgorisme, int longitudClau, String algorismeHash, String tipusXifrat) {
		
		String textTest = "Prova d'encriptacio";
		System.out.println(tipusAlgorisme+ " - " + longitudClau);
		SecretKey clau = PovFraUF1A1EncryptData.passwordKeyGeneration("atenea", longitudClau, algorismeHash, tipusAlgorisme);
		byte[] encriptat = PovFraUF1A1EncryptData.transformData(clau, textTest.getBytes(), Cipher.ENCRYPT_MODE, tipusAlgorisme, tipusXifrat, tipusPadding);
		byte[] desencriptat = PovFraUF1A1EncryptData.transformData(clau, encriptat, Cipher.DECRYPT_MODE, tipusAlgorisme, tipusXifrat, tipusPadding);

		assertEquals(textTest, new String(desencriptat));
		
	}

}
